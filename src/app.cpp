#include <mutex>
#include <iostream>
#include "curl_functions.h"
#include "send_functions.h"
#include "json_processing.h"
#include <thread>
#include <chrono>
#include <stdio.h> 
#include <netdb.h> 
#include <netinet/in.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h> 
#include <unistd.h> // read(), write(), close()
#include <mutex>
#include <string>

static std::mutex data_mutex;
static std::mutex cout_mutex;
static SendMessages send_api(10000, "127.0.0.1");
static std::string recieved_message;

#define MAX 65000
#define REC_PORT 12345
#define SEND_PORT 12350
#define SA struct sockaddr 
char rec_buff[65000];
struct WeatherData{
    double average_temperature;
    double average_wind;
    double average_insolation;
};
typedef struct WeatherData WeatherData;

WeatherData weather_data = {0.0, 0.0, 0.0};

static void process_json(WeatherData* weather_data, JSONParser& json){
    int weather_data_size = json["data"]["weather_data"].as_array().size();

    /*Clear weather data*/
    weather_data->average_wind = 0.f;
    weather_data->average_insolation = 0.f;
    weather_data->average_temperature = 0.f;

    /*sum all values form data array*/
    for(int i =0; i < weather_data_size; i++){
        weather_data->average_wind += json["data"]["weather_data"].as_array()[i]["wind"].as_float();
        weather_data->average_insolation += json["data"]["weather_data"].as_array()[i]["insolation"].as_float();
        weather_data->average_temperature += json["data"]["weather_data"].as_array()[i]["temperature"].as_float();
    };
    /*Calculate average*/
    weather_data->average_wind /= weather_data_size;
    weather_data->average_insolation /= weather_data_size;
    weather_data->average_temperature /= weather_data_size;
};


void recieve_data_server(){
    data_mutex.lock();
    int port = REC_PORT;
    std::string pre_info = "thread rec: ";
    int sockfd, connfd, len; 
    struct sockaddr_in servaddr, cli; 
    sockfd = socket(AF_INET, SOCK_STREAM, 0); 
    if (sockfd == -1) { 
        std::cout<< pre_info << "Socket creation failed.." << std::endl;
        exit(0); 
    } 
    else{
        std::cout<<pre_info << "Socket successfully created.." << std::endl;
    }
    bzero(&servaddr, sizeof(servaddr)); 
   
    // assign IP, PORT 
    servaddr.sin_family = AF_INET; 
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY); 
    servaddr.sin_port = htons(port); 
   
    // Binding newly created socket to given IP and verification 
    if ((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) != 0) {
        std::cout<<pre_info << "socket bind failed..." << std::endl; 
        exit(0); 
    } 
    else
        std::cout<<pre_info << "Socket successfully binded.." << std::endl; 
   
    // Now server is ready to listen and verification 
    if ((listen(sockfd, 5)) != 0) { 

        std::cout<<pre_info << "Listen failed..." << std::endl; 
        exit(0); 
    } 
    else
        std::cout<<pre_info << "Server listening.." << std::endl; 
    len = sizeof(cli); 
   
    // Accept the data packet from client and verification 
    connfd = accept(sockfd, (SA*)&cli, (socklen_t*)&len); 
    if (connfd < 0) { 
        std::cout<<pre_info << "Server accept failed..." << std::endl; 
        exit(0); 
    } 
    else{
        std::cout<<pre_info << "server accept the client..." << std::endl; 
    }
    data_mutex.unlock();

    while(true) { 
        data_mutex.lock();
        bzero(rec_buff, MAX);
        read(connfd, rec_buff, sizeof(rec_buff));
        std::string data_string = rec_buff;
        JSONParser json_data(data_string);
        process_json(&weather_data, json_data);
        data_mutex.unlock();
        std::cout << "wind: "<< weather_data.average_wind<<std::endl;
        std::cout << "temperature: "<< weather_data.average_temperature<<std::endl;
        std::cout << "insolation: "<< weather_data.average_insolation<<std::endl;
        if (strncmp("exit", rec_buff, 4) == 0) { 
            printf("Server Exit...\n"); 
            break; 
        } 
    }
    close(sockfd); 
}



void send_data_server(){
     // socket create and verification
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    SendMessages send_api(SEND_PORT, "0.0.0.0"); // establish connection to database on given port, at given IP
    try{send_api.establish_connection();}
    catch (const SendException &err){
        std::cout << "Failed to connect at startup, message: " << err.what() << std::endl;
    }
    // int port = SEND_PORT;
    // std::string pre_info = "thread send: ";
    // int sockfd, connfd, len; 
    // struct sockaddr_in servaddr, cli; 
    // sockfd = socket(AF_INET, SOCK_STREAM, 0); 
    // if (sockfd == -1) { 
    //     std::cout<< pre_info << "Socket creation failed.." << std::endl;
    //     exit(0); 
    // } 
    // else{
    //     std::cout<<pre_info << "Socket successfully created.." << std::endl;
    // }
    // bzero(&servaddr, sizeof(servaddr)); 
   
    // // assign IP, PORT 
    // servaddr.sin_family = AF_INET; 
    // servaddr.sin_addr.s_addr = inet_addr("127.0.0.1"); 
    // servaddr.sin_port = htons(port); 
   
    // // Binding newly created socket to given IP and verification 
    // if (connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0){
    //     std::cout<<pre_info << "socket connect failed..." << std::endl; 
    //     exit(0); 
    // } 
    // else
    //     std::cout<<pre_info << "Socket successfully connected.." << std::endl; 
   
    // Now server is ready to listen and verification 
    // if ((listen(sockfd, 5)) != 0) { 

    //     std::cout<<pre_info << "Listen failed..." << std::endl; 
    //     exit(0); 
    // } 
    // else
    //     std::cout<<pre_info << "Server listening.." << std::endl; 
    // len = sizeof(cli); 
   
    // Accept the data packet from client and verification 
    // connfd = accept(sockfd, (SA*)&cli, (socklen_t*)&len); 
    // if (connfd < 0) { 
    //     std::cout<<pre_info << "Server accept failed..." << std::endl; 
    //     exit(0); 
    // } 
    // else{
    //     std::cout<<pre_info << "server accept the client..." << std::endl; 
    // }

    char transmit_buff[MAX] = "Ociechuj\n";
    while(true) { 
        bzero(transmit_buff, sizeof(transmit_buff));
        data_mutex.lock();
        sprintf(transmit_buff, "{\"temperature\":%0.2f,\"wind\":%0.2f,\"insolation\":%0.2f} ", weather_data.average_temperature, weather_data.average_wind, weather_data.average_insolation);
        std::string cos(transmit_buff);
        std::cout<< "Sending data"<<std::endl;

        try {send_api.send_message(cos);}
        catch (const SendException &err) {
        if(err.get_error_id() == 0 || err.get_error_id() == 1){
            std::cout << "Failed to establish connection, message: " << err.what() << std::endl;
        }
        if(err.get_error_id() == 2){
            std::cout << "Failed to send, abolished connection, message: " << err.what() << std::endl;
        }
    }
        //write(connfd, cos.c_str(), cos.length());
        std::cout<< "Data send"<<std::endl;
        data_mutex.unlock();
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
    //close(sockfd); 
    
}
