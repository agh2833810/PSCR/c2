#include "send_functions.h"

SendMessages::~SendMessages() {
    close(sockfd_);
}

void SendMessages::send_message(std::string &msg){
    if(!opened_) {
        std::cout << "Not connected, trying to reconnect..." << std::endl;
        try {reconnect();}
        catch (const SendException& err){
            throw SendException(err.what(), err.get_error_id());
        }

    }
    if (send(sockfd_, msg.c_str(), msg.length(), 0) < 0) {
        close_();
        throw SendException("Failed to send data", 2);
    }
}

void SendMessages::reconnect() {
    if(opened_) close_();
    sockfd_ = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd_ < 0) {
        sockfd_ = -1;
        throw SendException("Failed to create socket", 0);
    }

    struct sockaddr_in serverAddr{};
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(port_);
    serverAddr.sin_addr.s_addr = inet_addr(host_);

    if (connect(sockfd_, (struct sockaddr *) &serverAddr, sizeof(serverAddr)) < 0) {
        close_();
        throw SendException("Failed to connect", 1);
    }
    opened_ = true;
}

void SendMessages::close_() {
    if(sockfd_ != -1) {
        close(sockfd_);
        sockfd_ = -1;
        opened_ = false;
    }
}
