#include <iostream>
#include <thread>
#include <semaphore>
#include <algorithm>
#include <mutex>
#include "curl/curl.h"
#include "app.hpp"

extern void recieve_data_server();
extern void send_data_server();  




int main() {
    std::thread recieve_thread(recieve_data_server);
    std::thread send_thread(send_data_server);
    recieve_thread.join();
    send_thread.join();
    std::cout << "Hello!\n";
    return EXIT_SUCCESS;
}
