# echo-client.py

import socket
import time

HOST = "127.0.0.1"  # The server's hostname or IP address
PORT = 12345  # The port used by the server

string_to_send = '''
{
    "data": {
        "weather_data": [
            {
                "temperature": 25,
                "insolation":  302,
		        "wind": 30,
                "direction": "SE"
            },
            {
                "temperature": 20.3,
                "insolation":  200,
		        "wind": 20,
                "direction": "SE"
            },
            {
                "temperature": 27.3,
                "insolation":  220,
		        "wind": 1,
                "direction": "SE"
            },
            {
                "temperature": 27.3,
                "insolation":  220,
		        "wind": 1,
                "direction": "SE"
            },
            {
                "temperature": 27.3,
                "insolation":  820,
		        "wind": 1,
                "direction": "SE"
            },
            {
                "temperature": 27.3,
                "insolation":  120,
		        "wind": 1,
                "direction": "SE"
            },
            {
                "temperature": 27.3,
                "insolation":  920,
		        "wind": 1,
                "direction": "SE"
            },
            {
                "temperature": 21.3,
                "insolation":  600,
		        "wind": 1,
                "direction": "SE"
            },
            {
                "temperature": 28.3,
                "insolation":  320,
		        "wind": 1,
                "direction": "SE"
            },
            
        ]
}
}
'''

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    for i in range(1000):
        s.sendall(bytes(string_to_send, 'ascii'))
        time.sleep(5)